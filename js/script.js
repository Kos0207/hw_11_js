let showPassword = (icon) => {
  return function () {
    icon.parentNode.querySelector(".d-none").classList.remove("d-none");
    icon.classList.add("d-none");

    const input = icon.parentNode.querySelector("input");
    if (input.getAttribute("type") == "password") {
      input.setAttribute("type", "text");
    } else {
      input.setAttribute("type", "password");
    }
  };
};

const icons = document.querySelectorAll(".icon-password");
icons.forEach((icon) => icon.addEventListener("click", showPassword(icon)));

const button = document.querySelector(".btn");
button.addEventListener("click", () => {
  const input1 = document.querySelector(".password");
  const input2 = document.querySelector(".password-confirm");
  const error = document.querySelector(".confirm-input-error");
  if (input1.value === input2.value) {
    error.textContent="";
    input1.value = input2.value = "";
    alert("You are welcome");
  } else {
    error.textContent="Потрібно ввести однакові значення";
    
  }
});

const form = document.querySelector("form");
form.addEventListener("submit", (e) => {
  e.preventDefault();
});
